import Vue from 'vue'
import Foo from '../../src/page/foo.vue'

describe('foo.vue', () => {

    it('should render correct contents', () => {
        const vm = new Vue({
            el: document.createElement('div'),
            render: (h) => h(Foo)
        })
        expect(vm.$el.querySelector('h2').textContent).toBe('Foo')
        const p_elems = vm.$el.querySelectorAll('p')
        expect(p_elems[1].textContent).toBe('Tada: Some fixed text')

        // eslint-disable-next-line no-console
        console.log('p_elems[0])', p_elems[0])
        // eslint-disable-next-line no-console
        console.log('p_elems[0]).textContent', p_elems[0].textContent)
        const result = p_elems[0].textContent.indexOf('Date:') == 0
        expect(result).toBeTruthy()
    })

})

// also see example testing a component with mocks at
// https://github.com/vuejs/vueify-example/blob/master/test/unit/a.spec.js#L22-L43
