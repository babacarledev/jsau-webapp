import AppContactLink from '../web-component/app-contact-link.vue'

export default {
    // Local component registration
    components: {
        AppContactLink
    },
    data() {
        return {
            date: new Date(),
            tada: 'Some fixed text',
            pane_text: ''
        }
    },
    methods: {
        doIt(evt) {
            this.pane_text = this.pane_text + 'Do. Or do not. There is no try. '
        }
    }
}
