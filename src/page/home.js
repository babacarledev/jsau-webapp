import AppContactLink from '../web-component/app-contact-link.vue'

export default {
    // Local component registration
    components: {
        AppContactLink
    }
}
